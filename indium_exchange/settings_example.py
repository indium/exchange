# IPDB
BDB_URL = 'https://test.ipdb.io'
BDB_TOKENS = {'app_id': 'app_id', 'app_key': 'app_key'}

# Infura
INFURA_URL = 'https://rinkeby.infura.io/key'

# SQLALCHEMY
SQLALCHEMY_DB = 'postgres://user:password@localhost/exchange' #postgres
SQLALCHEMY_TRACK_MODIFICATIONS = False

# Server
SERVER_NAME = 'Indium Exchange'
SERVER_HOST = '0.0.0.0'
SERVER_PORT = 5000
DEBUG = True
