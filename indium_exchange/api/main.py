from flask import render_template, request, url_for, redirect, Blueprint

from indium_exchange.models.ask import Ask
from indium_exchange.models.bids import Bid

from indium_exchange import db

v0 = Blueprint('v0', __name__, template_folder='templates')

general_data = {
    'platform_name': 'Indium Exchange',
    'org_name': 'Indium Foundation'
}


@v0.route("/", methods=['GET'])
def home():
    return render_template('home.html', title='Indium Exchange', general_data=general_data)


@v0.route("/dbsetup", methods=['GET'])
def dbsetup():
    db.create_all()
    return "DB created"


@v0.route("/assets/<asset_id>", methods=['GET'])
def show_asset(asset_id):
    bids = Bid.query.filter_by(asset_id=asset_id).all()
    asks = Ask.query.filter_by(asset_id=asset_id).all()
    asset_data = {
        'description': 'Asset Description'
    }
    return render_template('assets.html', asset_id=asset_id, asset_data=asset_data, bids=bids, asks=asks, title='Assets | Indium Exchange', general_data=general_data)


@v0.route("/addbid", methods=['POST'])
def addbid():
    bid = Bid.from_eth_txn(request.form['eth_txn_id'])
    return redirect(url_for('show_asset', asset_id=bid.asset_id))


@v0.route("/addask", methods=['POST'])
def addask():
    ask = Ask.from_ipdb_txn(request.form['ipdb_txn_id'])
    return redirect(url_for('show_asset', asset_id=ask.asset_id))
