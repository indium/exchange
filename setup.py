from setuptools import setup
from pip.req import parse_requirements
from pip.download import PipSession

install_reqs = parse_requirements('requirements.txt', session=PipSession())
reqs = [str(req.req) for req in install_reqs]

setup(name='indium_exchange',
      version='0.1.0',
      description='Exchange Tokens',
      url='http://indium.org.in/',
      author='hacktohell',
      author_email='',
      license='TODO',
      install_requires=reqs,
      package_dir={'indium_exchange': 'indium_exchange'},
      zip_safe=False)
